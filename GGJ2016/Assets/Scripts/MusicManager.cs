﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicManager : MonoBehaviour {
	public static MusicManager instance;
	public AudioClip[] tracks;
	public int playOnAwake = -1;
	public float crossfadeSpeed = 0.01f;

	private AudioSource as1;
	private AudioSource as2;
	private int currentTrack;
	private bool isAs2;
	private bool _isAs2;
	private List<AudioSource> sounds;

	// Use this for initialization
	void Awake () {
		if(instance != null) {
			Destroy(gameObject);
			return;
		}

		DontDestroyOnLoad(gameObject);
		instance = this;

		as1 = gameObject.AddComponent<AudioSource>();
		as2 = gameObject.AddComponent<AudioSource>();
		as1.loop = true;
		as2.loop = true;

		sounds = new List<AudioSource>();
		currentTrack = -1;

		if(playOnAwake != -1) {
			PlayTrack(playOnAwake);
		}
	}
	
	void Update () {
		if(_isAs2 != isAs2) {
			Vector2 current = new Vector2(as1.volume,as2.volume);
			Vector2 destination = new Vector2(0,1);
			if(isAs2) {
				destination = new Vector2(1,0);
			}
			Vector2 result = Vector2.MoveTowards(current,destination,crossfadeSpeed);

			as1.volume = result.x;
			as2.volume = result.y;

			if(result == destination) {
				_isAs2 = isAs2;
			}
		}
	}

	public static void PlayTrack(string name) {
		int i = 0;
		foreach(AudioClip clip in instance.tracks) {
			if(clip.name == name) {
				PlayTrack(i);
				return;
			}
			i++;
		}
		Debug.Log("WARNING: Could not find track: " + name);
	}

	public static void Stop() {
		if(instance.isAs2) {
			instance.as2.clip = null;
		} else {
			instance.as1.clip = null;
		}
		instance.currentTrack = -1;
		instance.isAs2 = !instance.isAs2;
	}

	public static void PlayTrack(int id) {
		if(instance == null) {
			Debug.Log("WARNING: MusicManager is not initiated");
			return;
		}

		if(id < 0 || id > instance.tracks.Length) {
			Debug.Log("WARNING: Could not find track: " + id);
			return;
		}

		if(instance.currentTrack != id) {
			AudioSource source = instance.as1;
			if(instance.isAs2) {
				source = instance.as2;
			}

			source.clip = instance.tracks[id];
			source.Play();
			instance.isAs2 = !instance.isAs2;

			instance.currentTrack = id;
		}
	}
}
