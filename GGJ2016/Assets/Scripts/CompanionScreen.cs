﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CompanionScreen : MonoBehaviour {
	public static CompanionScreen instance;
	public bool isServer;
	public string ip;
	public CompanionScreenSharedInfo sharedInfo;

	public GameObject IPSetter;
	public Text newIP;
	public GameObject Interface;

	// Use this for initialization
	void Awake () {
		if(instance != null) {
			Destroy(gameObject);
			return;
		}

		instance = this;	
		DontDestroyOnLoad(gameObject);

		if (Network.peerType == NetworkPeerType.Disconnected) {
			if(isServer) {
				ConnectAsServer();
			} else {
				IPSetter.SetActive(true);
				Interface.SetActive(false);
			}
		}
	}

	void OnGUI() {
		if(isServer) {
			if (Network.peerType != NetworkPeerType.Disconnected) {
				GUILayout.Label(Network.player.ipAddress);
			}
		}
	}

	public void SetIP() {
		ip = newIP.text;
		ConnectAsClient();
	}

	void ConnectAsServer ()
	{
		Network.InitializeServer (10, 25000, false);
	}
	void ConnectAsClient() {
		Network.Connect(ip, 25000);
	}

	public static void SendInfo() {
		if(Network.peerType == NetworkPeerType.Disconnected) {
			Debug.LogError("ERROR: Not connected!");
		} else {
			if(instance.isServer) {
				instance.SendInfoToClient();
			} else {
				instance.SendInfoToServer();
			}
		}
	}

	void OnApplicationQuit() {
		Network.Disconnect();
	}

	void OnPlayerConnected(NetworkPlayer player) {
		AskClientForInfo(player);
	}
	void AskClientForInfo(NetworkPlayer player) {
		GetComponent<NetworkView>().RPC("SetPlayerInfo", player, player);
	}

	void OnConnectedToServer() {
		Debug.Log("Connected to server");
		IPSetter.SetActive(false);
		Interface.SetActive(true);
	}

	void OnDiconnectedToServer() {
		Debug.Log("Disconnected from server");
		IPSetter.SetActive(true);
		Interface.SetActive(false);
	}

	[RPC]
	void RecieveInfoFromClient(string someInfo) {
		Debug.Log(someInfo);
	}

	[RPC]
	void SendInfoToClient() {
		GetComponent<NetworkView>().RPC("ReceiveInfoFromServer", RPCMode.Others, "Hello");
	}

	[RPC]
	void SendInfoToServer() {
		GetComponent<NetworkView>().RPC("ReceiveInfoFromClient", RPCMode.Server, "Hello");
	}
	[RPC]
	void SetPlayerInfo(NetworkPlayer player) {
		//sharedInfo = player;
		GetComponent<NetworkView>().RPC("ReceiveInfoFromClient", RPCMode.Server, "Settled");
	}
	[RPC]
	void ReceiveInfoFromServer(string someInfo) {
		Debug.Log(someInfo);
	}
}

public class CompanionScreenSharedInfo {
	public int health;
}