﻿using UnityEngine;
using System.Collections;

public class Lightning : MonoBehaviour {
	private Light light;

	[RangeAttribute(0,100)]
	public float frequency;

	private float timer;

	void Start() {
		light = GetComponent<Light>();
	}

	void Update () {
		if(LunarView.isPaused)
			return;
		
		if(timer < 0) {
			Strike();
			timer = Random.Range(0.1f, 2.0f);
		} else {
			timer -= Time.deltaTime;
		}

		light.intensity -= Time.deltaTime * 10.0f;
	}

	void Strike() {
		float r = Random.Range(0.0f, 100.0f);

		if(r > frequency) {
			light.intensity = 4;
			GetComponent<AudioSource>().pitch = Random.Range(0.9f, 1.1f);
			GetComponent<AudioSource>().Play((ulong)Random.Range(0.0f, 100.0f));
		}			
	}
}
