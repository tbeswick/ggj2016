﻿using UnityEngine;
using System.Collections;

public class CloudMovement : MonoBehaviour {
	public float distance;
	public float speed;
	private Vector3 origin;
	private float random;

	void Start() {
		origin = transform.localPosition;
		random = Random.Range(0.0f, 1.0f);
	}

	void Update () {
		Vector3 offset = new Vector3(distance * Mathf.Sin(Time.time * speed + random), distance * 0.2f * Mathf.Sin(Time.time * speed * 0.5f + random), 0);
		transform.localPosition = origin + offset;
	}
}
