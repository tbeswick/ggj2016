﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LunarView : MonoBehaviour {
	public static bool isPaused;

	public GameObject starsMenu;
	public GameObject escapeMenu;
	public GameObject encyclopedia;

	void Start() {
		starsMenu.SetActive(true);
		isPaused = true;
		MusicManager.PlayTrack(2);

		escapeMenu.SetActive(false);
		encyclopedia.SetActive(false);
	}

	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.F1) || Input.GetButtonDown("Fire2")) {
			if(encyclopedia.activeSelf) {
				HideEncyclopedia();
			} else if(escapeMenu.activeSelf) {
				HideMenu();
			} else if(isPaused) {
				HideStars();
			} else {
				ShowStars();
			}
		}	

		if(Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.LeftShift) || Input.GetButtonDown("Fire1")) {
			if(isPaused) {
				ShowMenu();
			}
		}	
	}

	void ShowStars() {
		//showStarFadeAnimation
		starsMenu.SetActive(true);
		starsMenu.GetComponent<Animator>().Play("Show");
		isPaused = true;
		MusicManager.PlayTrack(2);
	}

	void HideStars() {
		starsMenu.GetComponent<Animator>().Play("Hide");
		//showStarFadeAnimation
		isPaused = false;

		if(BadWeather.isRaining) {
			MusicManager.PlayTrack(1);
		} else {
			MusicManager.PlayTrack(0);
		}

	}

	void ShowMenu() {
		escapeMenu.SetActive(true);
	}

	public void ShowEncyclopedia() {
		escapeMenu.SetActive(false);
		encyclopedia.SetActive(true);
		Encyclopedia.Show(0);
	}

	public void HideEncyclopedia() {
		escapeMenu.SetActive(true);
		encyclopedia.SetActive(false);
	}

	public void HideMenu() {
		escapeMenu.SetActive(false);
		HideStars();
	}
}
