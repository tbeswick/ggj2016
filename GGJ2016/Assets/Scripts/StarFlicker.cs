﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StarFlicker : MonoBehaviour {
	private Image img;
	private float speed;
	private float offset;

	// Use this for initialization
	void Start () {
		img = GetComponent<Image>();
		speed = Random.Range(0.9f, 2.1f);
		offset = Random.Range(0.0f, 10.0f);
	}
	
	// Update is called once per frame
	void Update () {
		Color c = img.color;

		c.a = (0.25f * Mathf.Sin((speed * Time.time) + offset)) + 0.75f;
		img.color = c;
	}
}
