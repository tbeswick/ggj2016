﻿using UnityEngine;
using System.Collections;

public class DayNight : MonoBehaviour {
	public enum daytime { Morning, Noon, Evening, Night }
	public static DayNight instance;
	public delegate void CycleChange (daytime t);
	public CycleChange OnChange;

	public float speed;

	[RangeAttribute(0,100)]
	public float hour;

	[RangeAttribute(0,400)]
	public float time;
	public Gradient color;

	private daytime cycleTime;

	// Use this for initialization
	void Awake () {
		instance = this;
		cycleTime = daytime.Morning;
	}
	
	// Update is called once per frame
	void Update () {
		if(LunarView.isPaused)
			return;

		hour += Time.deltaTime * speed;
		time += Time.deltaTime * speed;

		if(hour > 100) {
			hour -= 100;

			if(cycleTime == daytime.Morning) {
				cycleTime = daytime.Noon;
			} else if(cycleTime == daytime.Noon) {
				cycleTime = daytime.Evening;
			} else if(cycleTime == daytime.Evening) {
				cycleTime = daytime.Night;
			} else if(cycleTime == daytime.Night) {
				cycleTime = daytime.Morning;
				time -= 400;
			}

			if(OnChange != null) {
				OnChange(cycleTime);
			}
		}			

		RenderSettings.ambientLight = color.Evaluate(time / 400);
	}		

	public static daytime GetTime() {
		return instance.cycleTime;
	}
}
