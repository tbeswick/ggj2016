﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Image))]
public class Splashscreen : MonoBehaviour {
	private Image img;
	private Color clear;
	private float timer;
	private float fadeDur;
	private int current;

	public float durationPerImage;
	public Sprite[] images;
	public int nextScene;
	public int nextSceneMobile;

	void Start () {
		img = GetComponent<Image>();
		clear = Color.white;
		clear.a = 0;
		timer = 0;
		current = -1;
		fadeDur = durationPerImage / 3;

		NewImg();
	}

	void Update () {
		if(Input.anyKey) {
			current = 99;
			NewImg();
		}

		timer += Time.deltaTime;

		if(timer < fadeDur) {
			img.color = Color.Lerp(clear, Color.white, timer / fadeDur);
		} else if(timer > durationPerImage - fadeDur) {
			img.color = Color.Lerp(Color.white, clear, (timer - (durationPerImage - fadeDur)) / fadeDur);
		} else {
			img.color = Color.white;
		}

		if(timer > durationPerImage) {
			NewImg ();
		}
	}

	void NewImg() {
		timer = 0;
		current++;

		if(current >= images.Length) {
			if(Input.GetKey(KeyCode.Home)) {
				Application.LoadLevel(nextSceneMobile);
			} else {
				Application.LoadLevel(nextScene);
			}
		} else {
			img.sprite = images[current];
		}
	}
}
