﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LunaCycle : MonoBehaviour {
	public Sprite[] stages;
	private int currentStage;

	// Use this for initialization
	void Start () {
		currentStage = Random.Range(0,stages.Length);
		DayNight.instance.OnChange += NewCycle;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Backspace)) {
			NewDay();
		}
	}

	void NewCycle(DayNight.daytime night) {
		if(night == DayNight.daytime.Morning) {
			NewDay();
		}
	}

	public void NewDay() {
		currentStage++;
		if(currentStage >= stages.Length) {
			currentStage = 0;
		}

		GetComponent<Image>().sprite = stages[currentStage];
	}
}
