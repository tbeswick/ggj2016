﻿using UnityEngine;
using System.Collections;

public class DependOnCycle : MonoBehaviour {
	public DayNight.daytime activeFrom;
	public DayNight.daytime disabledFrom;

	void Start () {
		DayNight.instance.OnChange += OnChange;
		gameObject.SetActive(activeFrom == DayNight.GetTime());
	}
	
	void OnChange (DayNight.daytime n) {
		if(activeFrom == n)
			gameObject.SetActive(true);
		if(disabledFrom == n)
			gameObject.SetActive(false);
	}
}
