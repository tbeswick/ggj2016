﻿using UnityEngine;
using System.Collections;

public class BadWeather : MonoBehaviour {
	public static bool isRaining;

	public ParticleSystem rain;
	public ParticleSystem storm;
	public GameObject lightning;
	public GameObject puddles;

	[RangeAttribute(0,100)]
	public float stormChance = 75.0f;

	public bool force;

	private bool stormWarning;

	void Start () {
		rain.Stop();
		storm.Stop();
		puddles.SetActive(false);
		lightning.SetActive(false);

		DayNight.instance.OnChange += OnCycleChange;

		Terrain t = null;
		RitualSite.OnRitual += OnRitual;
	}

	void OnRitual(RitualSite.RitualType type) {
		if(type == RitualSite.RitualType.Storms) {
			stormWarning = false;
			rain.Stop();
			storm.Stop();
			puddles.SetActive(false);
			lightning.SetActive(false);
			isRaining = false;
			MusicManager.PlayTrack(0);
		}
	}

	void OnCycleChange(DayNight.daytime d) {
		if(stormWarning) {
			if(d == DayNight.daytime.Noon) {
				rain.Play();
				isRaining = true;
				MusicManager.PlayTrack(1);
			}
			else if(d == DayNight.daytime.Evening) {
				storm.Play();
				puddles.SetActive(true);
			}
			else if(d == DayNight.daytime.Night) {
				lightning.SetActive(true);
			}
			else if(d == DayNight.daytime.Morning) {
				lightning.SetActive(false);
				storm.Stop();
				puddles.SetActive(false);
				stormWarning = false;
			}
		} else {
			if(d == DayNight.daytime.Morning) {
				float r = Random.Range(0.0f, 100.0f);
				if(r > stormChance || force) {
					stormWarning = true;
					force = false;
				}
			}

			if(d == DayNight.daytime.Noon) {
				rain.Stop();
				isRaining = false;
				MusicManager.PlayTrack(0);
			}
		}


	}
}
