﻿using UnityEngine;
using System.Collections;

public class CollisionObject : MonoBehaviour {
	public GameObject[] children;
	public float spawnDistance;

	private int childCount;
	private Vector3[] poss;

	public void Start() {
		childCount = children.Length -1;
		poss = new Vector3[children.Length];
		for(int i = 0; i < children.Length; i++) {
			poss[i] = children[i].transform.position;
		}
		RitualSite.OnRitual += OnRitual;
	}

	void OnRitual(RitualSite.RitualType type) {
		if(type == RitualSite.RitualType.Fertility) {
			for(int i = 0; i < children.Length; i++) {
				children[i].GetComponent<Rigidbody>().isKinematic = true;
				children[i].transform.position = poss[i];
				children[i].SetActive(true);
			}
			childCount = children.Length -1;
		}
	}

	public void Damage() {		
		if(childCount >= 0) {
			children[childCount].GetComponent<Rigidbody>().isKinematic = false;
			childCount--;

			//float r = Random.Range(0, 360) * Mathf.Deg2Rad;
			//Vector3 offset = new Vector3(Mathf.Cos(r), 0, Mathf.Sin(r)) * spawnDistance;
			//g.transform.position = transform.position + offset;
		}
	}
}
