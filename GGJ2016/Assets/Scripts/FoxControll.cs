﻿using UnityEngine;
using System.Collections;

public class FoxControll : MonoBehaviour {
	public delegate void GainedUpgrade();
	public GainedUpgrade OnUpgrade;

	NavMeshAgent agent;
	UnityStandardAssets.Characters.ThirdPerson.AICharacterControl ai;

	public ParticleSystem dashEffect;
	public float dashDistance = 2;
	public int upgrades;

	public GameObject carrying;
	public Transform mouth;

	void Start() {
		ai = GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>();
		RitualSite.OnRitual += OnRitual;
		ai.SetTarget(transform.position);
	}

	void OnRitual(RitualSite.RitualType type) {
		if(type == RitualSite.RitualType.Destiny) {
			upgrades++;

			if(upgrades == 1) {
				GetComponent<NavMeshAgent>().speed += 1.0f;
			} else if(upgrades == 2) {
				dashDistance += 3;
			} else if(upgrades == 3) {
				GetComponent<NavMeshAgent>().speed += 1.0f;			
			} else if(upgrades == 5) {
				MusicManager.PlayTrack(3);
				Application.LoadLevel(2);
			}

			if(OnUpgrade != null) {
				OnUpgrade();
			}
		}
	}

	// Update is called once per frame
	void Update () {
		if(LunarView.isPaused)
			return;
		
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");

		if(h != 0 || v != 0) {
				ai.SetTarget(transform.position + new Vector3(h, 0, v));
		}

		if(Input.GetButtonDown("Fire1")) {
			if(h != 0 || v != 0) {
				dashEffect.Play();

					Ray ray = new Ray(transform.position, transform.forward);
					RaycastHit hit;
					if(Physics.Raycast(ray, out hit, dashDistance)) {
						GetComponent<NavMeshAgent>().Warp(hit.point);		
						hit.collider.SendMessage("Damage", SendMessageOptions.DontRequireReceiver);
					} else {
						GetComponent<NavMeshAgent>().Warp(transform.position + (ray.direction * dashDistance));

					RaycastHit[] hits = Physics.SphereCastAll(ray, 1.0f, dashDistance);
						if(hits != null && hits.Length > 0) {
							foreach(RaycastHit ht in hits) {
								ht.collider.SendMessage("Damage", SendMessageOptions.DontRequireReceiver);
							}
						}
					}					
			} else {
				if(carrying != null) {
					carrying.transform.parent = null;
					carrying.GetComponent<Rigidbody>().isKinematic = false;
					carrying.GetComponent<Collider>().enabled = true;
					carrying = null;
				} else {
					//pickup
					RaycastHit[] hits = Physics.SphereCastAll(transform.position+transform.forward, 1.0f, transform.forward, 0.1f);
					if(hits != null && hits.Length > 0) {
						foreach(RaycastHit ht in hits) {
							if(ht.collider.tag == "Carryable") {
								carrying = ht.collider.gameObject;
								ai.PlayAnim("idle_eat", false);
							}
						}
					}
				}
			}
		}
	}

	public void DoGrab() {
		carrying.GetComponent<Rigidbody>().isKinematic = true;
		carrying.transform.parent = mouth;
		carrying.GetComponent<Collider>().enabled = false;
		carrying.transform.localPosition = Vector3.zero;
	}
}
