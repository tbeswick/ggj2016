﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	public Rigidbody target;
	private Vector3 offset;

	public float smoothing = 1;

	private float desiredZoom;

	// Use this for initialization
	void Start () {
		offset = transform.position - target.position;
		desiredZoom = GetComponent<Camera>().fieldOfView;
	}
	
	// Update is called once per frame
	void Update () {
		iTween.MoveUpdate(gameObject, target.transform.position + offset, smoothing);

		GetComponent<Camera>().fieldOfView = iTween.FloatUpdate(GetComponent<Camera>().fieldOfView, desiredZoom, 1);
	}

	public void SetZoom(float zoom) {
		GetComponent<Camera>().fieldOfView = iTween.FloatUpdate(GetComponent<Camera>().fieldOfView, zoom, 2);
	}
}
