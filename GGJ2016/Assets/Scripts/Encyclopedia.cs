﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Encyclopedia : MonoBehaviour {
	public static Encyclopedia instance;
	public Sprite[] images;
	public string[] titles;
	public string[] entries;
	public Image img;
	public Text title;
	public Text txt;
	public ScrollRect scroll;

	// Use this for initialization
	void Awake () {
		instance = this;
		Show(0);
	}

	public static void Show(int id) {
		instance.img.sprite = instance.images[id];
		instance.title.text = instance.titles[id];
		instance.txt.text = instance.entries[id];
		instance.scroll.verticalNormalizedPosition = 0;
	}
}
