﻿using UnityEngine;
using System.Collections;

public class ButtonResponses : MonoBehaviour {

	public void LoadLevel (int id) {
		LunarView.isPaused = false;
		Application.LoadLevel(id);
	}

	public void LoadLevel (string id) {
		LunarView.isPaused = false;
		Application.LoadLevel(id);
	}
	
	public void QuitGame () {
		Application.Quit();
	}

	public void ShowEncyclopedia(int entry) {
		Encyclopedia.Show(entry);
	}
}
