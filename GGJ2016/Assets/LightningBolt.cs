﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class LightningBolt : MonoBehaviour {
	public bool oneShot;
	public Vector3 end;
	public float speed = 10;
	public float length = 4;

	private LineRenderer lr;
	private float time;

	void Start() {
		time = -(length * 0.1f);
	}

	public void ShootAt(Vector3 destination) {
		end = transform.worldToLocalMatrix.MultiplyPoint(destination);
		time = -(length * 0.1f);
		GetComponent<AudioSource>().pitch = Random.Range(0.9f, 1.1f);
		GetComponent<AudioSource>().Play();
	}

	// Update is called once per frame
	void Update () {
		if(lr == null)
			lr = GetComponent<LineRenderer>();
		if(LunarView.isPaused)
			return;

		if(!oneShot) {
			BetweenPoints(Vector3.zero, end);
		} else if(Application.isPlaying) {
			if(time >= 1) {
				Destroy(gameObject);
			}

			Vector3 start = Vector3.Lerp(Vector3.zero, end, time);
			Vector3 stop = Vector3.Lerp(Vector3.zero, end, time + (length * 0.1f));

			BetweenPoints(start, stop);

			float dist = Vector3.Distance(transform.position, end);
			time += (Time.deltaTime / dist) * speed;
		}
	}

	void BetweenPoints(Vector3 origin, Vector3 destination) {
		int segments = Mathf.RoundToInt(Vector3.Distance(origin, destination));
		segments = Mathf.Max(3, segments);

		lr.SetVertexCount(segments);
		Vector3[] points = new Vector3[segments];
		for(int i = 0; i < segments; i++) {
			float x = Random.Range(-0.1f,0.1f);
			float y = Random.Range(-0.1f,0.1f);
			float z = Random.Range(-0.1f,0.1f);

			if(i==0) {				
				if(origin != Vector3.zero) {
					points[i] = origin + new Vector3(x,y,z);
				} else {
					points[i] = origin;
				}
			} else if(i==segments-1) {				
				if(destination != end) {
					points[i] = destination + new Vector3(x,y,z);
				} else {
					points[i] = destination;
				}
			} else {				
				points[i] = Vector3.Lerp(origin, destination, (float)i / (float)segments) + new Vector3(x,y,z);
			}
		}
		lr.SetPositions(points);
	}
}
