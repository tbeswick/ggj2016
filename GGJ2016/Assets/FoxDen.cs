﻿using UnityEngine;
using System.Collections;

public class FoxDen : MonoBehaviour {
	public AudioClip[] clips;

	private AudioSource audioSource;
	private float t;

	void Start() {
		audioSource = GetComponentInChildren<AudioSource>();

		foreach(Animator a in GetComponentsInChildren<Animator>()) {
			a.SetTime((double)Random.Range(0.0f, 0.5f));
		}
	}

	// Update is called once per frame
	void Update () {
		if(LunarView.isPaused)
			return;
		
		if(t<=0) {
			t = Random.Range(3.0f, 4.0f);
			audioSource.pitch = Random.Range(0.9f, 1.1f);
			audioSource.clip = clips[Random.Range(0, clips.Length)];
			audioSource.Play();

			foreach(Animator a in GetComponentsInChildren<Animator>()) {
				float r = Random.Range(0, 100);
				if(r > 50)
					a.Play("roar");
			}
		} else {
			t-=Time.deltaTime;
		}
	}
}
