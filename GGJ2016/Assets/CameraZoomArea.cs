﻿using UnityEngine;
using System.Collections;

public class CameraZoomArea : MonoBehaviour {
	public float range;
	public float zoom;

	// Use this for initialization
	void OnDrawGizmos () {
		Gizmos.DrawWireSphere(transform.position, range);
	}
	
	// Update is called once per frame
	void Update () {
		float distance = Vector3.Distance(Component.FindObjectOfType<FoxControll>().transform.position, transform.position);

		if(distance < range) {
			Component.FindObjectOfType<CameraController>().SetZoom(zoom);
		}
	}
}
