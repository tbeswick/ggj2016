﻿using UnityEngine;
using System.Collections;

public class GGJSlideshow : MonoBehaviour {
	public GameObject[] pages;
	private int page = 0;

	// Use this for initialization
	void Start () {
		if(pages != null && pages.Length > 0) {
			foreach(GameObject g in pages) {
				g.SetActive(false);
			}
			pages[0].SetActive(true);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(pages != null && pages.Length > 0) {
			if(Input.anyKeyDown) {
				pages[page].SetActive(false);
				page++;

				if(page == pages.Length) {
					Application.LoadLevel(1);
				} else {
					pages[page].SetActive(true);
				}
			}
		} else {
			if(Input.GetKeyDown(KeyCode.F12)) {
				Application.LoadLevel(5);
			}
		}
	}
}
