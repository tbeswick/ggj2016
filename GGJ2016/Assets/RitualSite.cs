﻿using UnityEngine;
using System.Collections;

public class RitualSite : MonoBehaviour {	
	public enum RitualType { Fertility, Storms, Warding, Destiny }
	public RitualType type;

	public delegate void RitualActivated(RitualType type);
	public static RitualActivated OnRitual;

	public Vector3 offset;
	public float range;
	public ParticleSystem burst;
	public GameObject lightningBolt;
	public Renderer[] stones;
	public Material glowMat;
	public Gradient stoneGlow;
	private Color originalStoneColor;

	[RangeAttribute(0,100)]
	public float ritualProgress;

	private Transform fox;
	private float timer;
	private int counter;

	void Start() {
		fox = Component.FindObjectOfType<FoxControll>().gameObject.transform;
		originalStoneColor = stones[0].material.GetColor("_EmissionColor");
	}

	// Update is called once per frame
	void Update () {
		UpdateStones();

		float distance = Vector3.Distance(fox.position, transform.position + offset);
		if(distance < range) {	
			glowMat.SetColor("_EmissionColor", stoneGlow.Evaluate(Mathf.PingPong(Time.time, 1)));

			//TODO fix
			if(Input.GetKeyDown(KeyCode.F12)) {
				RitualSuccess();
			}
			if(RitualEvaluator.Evaluate(type)) {
				RitualSuccess();
			}
		} else if(distance < range + 5) {
			glowMat.SetColor("_EmissionColor", Color.black);
		}

		if(counter > 0) {
			timer-= Time.deltaTime;

			if(timer<0) {
				counter--;
				timer = 0.5f;
				Lightnings();
			}
		}
	}

	void UpdateStones() {
		ritualProgress = RitualEvaluator.currentProgress;
		if(ritualProgress < 20) {
			stones[0].material.SetColor("_EmissionColor", originalStoneColor);
			stones[1].material.SetColor("_EmissionColor", originalStoneColor);
			stones[2].material.SetColor("_EmissionColor", originalStoneColor);
			stones[3].material.SetColor("_EmissionColor", originalStoneColor);
		} else {			
			stones[0].material.SetColor("_EmissionColor", stoneGlow.Evaluate(Mathf.PingPong(Time.time, 1)));
		}

		if(ritualProgress > 40) {
			stones[1].material.SetColor("_EmissionColor", stoneGlow.Evaluate(Mathf.PingPong(Time.time, 1)));
		}
		if(ritualProgress > 60) {
			stones[2].material.SetColor("_EmissionColor", stoneGlow.Evaluate(Mathf.PingPong(Time.time, 1)));
		}
		if(ritualProgress > 80) {
			stones[3].material.SetColor("_EmissionColor", stoneGlow.Evaluate(Mathf.PingPong(Time.time, 1)));
		}
	}

	void RitualSuccess() {
		burst.Play();
		counter = 5;
		GetComponent<AudioSource>().Play();
		if(OnRitual != null) {
			OnRitual(type);
		}
	}

	void Lightnings() {
		for(int i = 0; i < 5; i++) {
			float r = Random.Range(0, 360) * Mathf.Deg2Rad;
			Vector3 offset = new Vector3(Mathf.Cos(r), 0, Mathf.Sin(r));

			GameObject g = (GameObject)Instantiate(lightningBolt, transform.position + Vector3.up, Quaternion.identity);
			g.GetComponent<LightningBolt>().ShootAt(transform.position + (offset * 10) + Vector3.up);
		}
	}

	void OnDrawGizmos() {
		Gizmos.DrawWireSphere(transform.position + offset, range);
	}
}
