﻿using UnityEngine;
using System.Collections;

public class RequireUpgrade : MonoBehaviour {
	public int upgradeIndex;
	public RectTransform lineImg;
	public RectTransform dest;

	// Use this for initialization
	void Start () {
		Component.FindObjectOfType<FoxControll>().OnUpgrade += OnUpgrade;
		gameObject.SetActive(false);
	}
	
	void OnUpgrade() {
		if(Component.FindObjectOfType<FoxControll>().upgrades == upgradeIndex) {
			gameObject.SetActive(true);
		}

		if(lineImg != null) {
			Vector3 differenceVector = dest.position - transform.position;

			lineImg.sizeDelta = new Vector2(differenceVector.magnitude, Mathf.PingPong(Time.time, 1) + 0.1f);
			lineImg.pivot = new Vector2(0, 0.5f);
			lineImg.position = transform.position;
			float angle = Mathf.Atan2(differenceVector.y, differenceVector.x) * Mathf.Rad2Deg;
			lineImg.rotation = Quaternion.Euler(0,0,angle);
		}
	}

}
