﻿using UnityEngine;
using System.Collections;

public class RitualEvaluator : MonoBehaviour {
	public const float timeNeeded = 10;
	public static float currentProgress;

	public static bool Evaluate(RitualSite.RitualType type) {
		if(type == RitualSite.RitualType.Fertility) {
			Fertility();
		}
		if(type == RitualSite.RitualType.Storms) {
			Seasons();
		}
		if(type == RitualSite.RitualType.Warding) {
			Warding();
		}
		if(type == RitualSite.RitualType.Destiny) {
			Destiny();
		}

		if(currentProgress >= 100) {
			currentProgress = 0;
			return true;
		}
		return false;
	}

	/// <summary>
	/// Dashing
	/// </summary>
	private static void Fertility() {
	}

	/// <summary>
	/// Konami code
	/// </summary>
	private static void Seasons() {
	}

	/// <summary>
	/// Spiral
	/// </summary>
	private static void Warding() {
	}

	/// <summary>
	/// Patience
	/// </summary>
	private static void Destiny() {
		if(Input.anyKey) {
			currentProgress = 0;
		}
		currentProgress += Time.deltaTime * timeNeeded;
	}
}
