﻿using UnityEngine;
using System.Collections;

public class CreatureAI : MonoBehaviour {
	public float viewRange;
	public bool hostile;

	private Transform fox;
	private NavMeshAgent agent;
	private UnityStandardAssets.Characters.ThirdPerson.AICharacterControl ai;
	private UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter tpc;

	// Use this for initialization
	void Start () {
		fox = Component.FindObjectOfType<FoxControll>().transform;
		ai = GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>();
		tpc = GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter>();
		agent = GetComponent<NavMeshAgent>();

		ai.SetTarget(transform.position);
	}
	
	// Update is called once per frame
	void Update () {
		float distance = Vector3.Distance(transform.position, fox.position);

		if(distance < viewRange) {
			if(hostile) {
				ai.SetTarget(fox.position);
			} else {
				transform.LookAt(fox);
				ai.SetTarget(transform.position - (transform.forward * 5));
				transform.LookAt(transform.position - transform.forward);
			}
		}
	}

	void Damage() {
		GetComponent<Rigidbody>().constraints = new RigidbodyConstraints();
		GetComponent<Animator>().enabled = false;
		agent.enabled = false;
		ai.enabled = false;
		tpc.enabled = false;
		this.enabled = false;
	}

	void OnDrawGizmos() {
		Gizmos.DrawWireSphere(transform.position, viewRange);
	}
}
